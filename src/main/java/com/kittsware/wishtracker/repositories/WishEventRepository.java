package com.kittsware.wishtracker.repositories;

import com.kittsware.wishtracker.entities.Participant;
import com.kittsware.wishtracker.entities.User;
import com.kittsware.wishtracker.entities.WishEvent;
import com.kittsware.wishtracker.entities.WishItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface WishEventRepository extends JpaRepository<WishEvent, Long> {

    Optional<WishEvent> findWishEventByWishEventId(Long wishEventId);

    long deleteWishEventsByWishEventOwnerName(String wishEventOwnerName);

    //List<WishEvent> findWishEventsByWishEventParticipantsIsContaining(Participant participant);
    Optional<WishEvent> findWishEventByWishEventParticipantsIsContaining(Participant participant);
}
