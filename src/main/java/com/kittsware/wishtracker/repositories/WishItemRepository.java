package com.kittsware.wishtracker.repositories;

import com.kittsware.wishtracker.entities.Participant;
import com.kittsware.wishtracker.entities.WishEvent;
import com.kittsware.wishtracker.entities.WishItem;
import com.kittsware.wishtracker.entities.adapters.WishItemOnly;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Repository
public interface WishItemRepository extends JpaRepository<WishItem, Long> {
    //Get fully qualified list for the non-owner
    Optional<List<WishItem>> findAllByWishItemOwner(Long wishItemOwner);

    Collection<WishItemOnly> findWishItemsByWishItemOwner(Long wishItemOwner);

    //List<WishItem> findAllByWishItemEvent(WishEvent wishEvent);
    List<WishItem> findAllByWishItemParticipant(Participant participant);

    Optional<List<WishItem>> findAllByWishItemBuyer(Long wishItemBuyer);
}
