package com.kittsware.wishtracker.repositories;

import com.kittsware.wishtracker.entities.Participant;
import com.kittsware.wishtracker.entities.WishEvent;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ParticipantRepository extends JpaRepository<Participant, Long> {

    //I need to get Participant by Username&EventId
    Optional<Participant> getParticipantByParticipantNameAndParticipantEvent(String participantName, WishEvent participantEvent);

    long deleteParticipantsByParticipantName(String participantName);

    List<Participant> findParticipantsByParticipantName(String participantName);

    //Should find all Participants for a single event be a Participant function or Wish Event?
    //Given the Event ID, get the Participant List. You need Wish Event Service to get the Event.
    Optional<List<Participant>> findParticipantsByParticipantEvent(WishEvent wishEvent);

    long deleteParticipantByParticipantNameAndParticipantEvent(String participantName, WishEvent wishEvent);
}
