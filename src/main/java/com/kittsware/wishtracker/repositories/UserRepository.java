package com.kittsware.wishtracker.repositories;

import com.kittsware.wishtracker.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    Optional<User> findUserByUsername(String username);

    //The long return value is number of deletions that occurred.
    long deleteByUsername(String username);
}
