package com.kittsware.wishtracker.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class WishItem implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long wishItemId;

    private String wishItemName;
    private Double wishItemPrice;
    private Boolean isBought = false;

    //These will be expanded upon once the User class is implemented
    private Long wishItemOwner;
    private Long wishItemBuyer;

    /*@JsonBackReference
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "wish_event_id")
    private WishEvent wishItemEvent;*/

    @JsonBackReference
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "participant_id")
    private Participant wishItemParticipant;

    public WishItem() {}

    public Long getWishItemId() {
        return wishItemId;
    }

    public void setWishItemId(Long wishItemId) {
        this.wishItemId = wishItemId;
    }

    public String getWishItemName() {
        return wishItemName;
    }

    public void setWishItemName(String wishItemName) {
        this.wishItemName = wishItemName;
    }

    public Double getWishItemPrice() {
        return wishItemPrice;
    }

    public void setWishItemPrice(Double wishItemPrice) {
        this.wishItemPrice = wishItemPrice;
    }

    public Boolean getBought() {
        return isBought;
    }

    public void setBought(Boolean bought) {
        isBought = bought;
    }

    public Long getWishItemOwner() {
        return wishItemOwner;
    }

    public void setWishItemOwner(Long wishItemOwner) {
        this.wishItemOwner = wishItemOwner;
    }

    public Long getWishItemBuyer() {
        return wishItemBuyer;
    }

    public void setWishItemBuyer(Long wishItemBuyer) {
        this.wishItemBuyer = wishItemBuyer;
    }

    /*public WishEvent getWishItemEvent() {
        return wishItemEvent;
    }

    public void setWishItemEvent(WishEvent wishItemEvent) {
        this.wishItemEvent = wishItemEvent;
    }*/

    public Participant getWishItemParticipant() {
        return wishItemParticipant;
    }

    public void setWishItemParticipant(Participant wishItemParticipant) {
        this.wishItemParticipant = wishItemParticipant;
    }
}
