package com.kittsware.wishtracker.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Participant {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long participantId;

    //This should map to a username in the User table
    //Steve can be present in Event1 and Event 2, so in the Participant Table there will be numerous rows for the same person
    //However a single Participant can't be present in multiple events. The combo of ParticipantName & EventID is unique
    private String participantName;

    @JsonBackReference
    //This is the real cause for error. Lazy loading won't actually get the objects, just
    //@ManyToOne(fetch = FetchType.LAZY)
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "wish_event_id")
    private WishEvent participantEvent;

    @OneToMany(mappedBy = "wishItemParticipant", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<WishItem> participantWishItems = new ArrayList<>();

    public Participant() {}

    public Participant(String participantName) {
        this.participantName = participantName;
    }

    public Long getParticipantId() {
        return participantId;
    }

    public void setParticipantId(Long participantId) {
        this.participantId = participantId;
    }

    public String getParticipantName() {
        return participantName;
    }

    public void setParticipantName(String participantName) {
        this.participantName = participantName;
    }

    public WishEvent getParticipantEvent() {
        return participantEvent;
    }

    public void setParticipantEvent(WishEvent wishItemEvent) {
        this.participantEvent = wishItemEvent;
    }

    public List<WishItem> getParticipantWishItems() {
        return participantWishItems;
    }

    public void setParticipantWishItems(List<WishItem> wishEventWishItems) {
        this.participantWishItems = wishEventWishItems;
    }
}
