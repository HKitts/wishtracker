package com.kittsware.wishtracker.entities.adapters;

public interface WishItemOnly {
    Long getWishItemId();
    String getWishItemName();
    Double getWishItemPrice();
    Long getWishItemOwner();
}
