package com.kittsware.wishtracker.entities;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class WishEvent {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long wishEventId;
    private String wishEventName;
    private String wishEventDescription;

    /*
        Should there be a Wrapper class called Participant that could function as a link for User / Event?
        The problem is right now I can see the ID, Name, Description, and Owner ID. I don't see Participants.
        However, when I get the User, I can see the Events they're a part of.

        I'm starting to think you shouldn't have any reference to the USER at all, at least not the object itself.
        Ideally you just have some way of verifying

        What if you had a Participant table, the object would be the UserID and the EventID
     */

    //TODO: Refactor the wishEventOwner to be a User class with proper mapping
    //Should we do the ManyToOne?
    //private Long wishEventOwner;
    /*@JsonBackReference
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(foreignKey = @ForeignKey(name = "user_id"), name = "user_id")
    private User wishEventOwner;*/
    private String wishEventOwnerName;
    //TODO: Refactor the wishEventOwner to be an ID only.

    //List of WishItems
    //Why did we comment this out? I think because we didnt want to get the wish items when we get the Event, because
    //that would include Wish Items you own that have all data showing
    //I dont think we care for Participants?
    //We do care for Participants. Now we need to think about how we want to handle
    /*@OneToMany(mappedBy = "wishItemEvent", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<WishItem> wishEventWishItems = new ArrayList<>();*/

    @OneToMany(mappedBy = "participantEvent", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Participant> wishEventParticipants = new ArrayList<>();

    //TODO: Refactor current participant list to be just User IDs
    /*
        For some reason the participant is removed after being added. But the bigger issue is that we are getting a User object
        returned to begin with. This is a huge security risk. Instead we should store a List of Username types; usernames are public
        for the most part. You have to know your Friend's username to find them to add to your Event.

        The big issue here is that you are no longer using the Mappings provided to you by Spring, which means you have to carefully
        think about what your system does whenever information is deleted. Events and Users will no longer be managed by Spring,
        which means if you delete a User, it won't automatically remove Events that the User Owned. This is okay though because
        managing the participants piece will be difficult.
     */
    /*@JsonBackReference
    @ManyToMany(mappedBy = "userWishEvents")
    private List<User> participants;*/

    public WishEvent() { }

    //List of participating Users.
    //TODO: Make sure when an event gets created, the owner is added to the List of participating Users
    //Should the participating User list be their IDs or actual Objects? I think it will be full Objects because that's how the Mapping works in Spring

    public Long getWishEventId() {
        return wishEventId;
    }

    public void setWishEventId(Long wishEventId) {
        this.wishEventId = wishEventId;
    }

    public String getWishEventName() {
        return wishEventName;
    }

    public void setWishEventName(String wishEventName) {
        this.wishEventName = wishEventName;
    }

    public String getWishEventDescription() {
        return wishEventDescription;
    }

    public void setWishEventDescription(String wishEventDescription) {
        this.wishEventDescription = wishEventDescription;
    }

    /*public User getWishEventOwner() {
        return wishEventOwner;
    }

    public void setWishEventOwner(User wishEventOwner) {
        this.wishEventOwner = wishEventOwner;
    }*/

    /*public List<WishItem> getWishEventWishItems() {
        return wishEventWishItems;
    }

    public void setWishEventWishItems(List<WishItem> eventWishItems) {
        this.wishEventWishItems = eventWishItems;
    }*/

    /*public List<User> getParticipants() {
        return participants;
    }

    public void setParticipants(List<User> participants) {
        this.participants = participants;
    }*/

    public String getWishEventOwnerName() {
        return wishEventOwnerName;
    }

    public void setWishEventOwnerName(String wishEventOwnerId) {
        this.wishEventOwnerName = wishEventOwnerId;
    }

    public List<Participant> getWishEventParticipants() {
        return wishEventParticipants;
    }

    public void setWishEventParticipants(List<Participant> wishEventParticipants) {
        this.wishEventParticipants = wishEventParticipants;
    }
}
