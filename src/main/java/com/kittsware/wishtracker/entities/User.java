package com.kittsware.wishtracker.entities;

import javax.persistence.*;
import java.util.List;

@Entity
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private String userId;

    private String username;
    private String password;

    @OneToMany(mappedBy = "user", fetch = FetchType.EAGER)
    private List<Authority> authorities;

    //This will need changed to just contain Events where the User is a participant, regardless of ownership of the event
    /*
        This User class does not care about what Events you are the owner of. It does care about events you are a participant in
     */
    //TODO: Refactor so that this is mapped to events that you are a participant in, not owner
    /*
        I think the wishEvents will be a @ManyToMany annotation instead of @OneToMany.
        An Event has many Users as Participants
        A User has many Events

        User will be the Owner of the relationship

     */
    //TODO: You should not be able to get a User Object from an Event.
    /*@ManyToMany(cascade = CascadeType.ALL)
    @JoinTable
    private List<WishEvent> userWishEvents;*/

    /*@OneToMany(mappedBy = "wishEventOwner", fetch = FetchType.LAZY)
    private List<WishEvent> wishEvents;*/

    public User() {}

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<Authority> getAuthorities() {
        return authorities;
    }

    public void setAuthorities(List<Authority> authorities) {
        this.authorities = authorities;
    }

    /*public List<WishEvent> getWishEvents() {
        return wishEvents;
    }

    public void setWishEvents(List<WishEvent> wishEvents) {
        this.wishEvents = wishEvents;
    }*/

    /*public List<WishEvent> getUserWishEvents() {
        return userWishEvents;
    }

    public void setUserWishEvents(List<WishEvent> userWishEvents) {
        this.userWishEvents = userWishEvents;
    }*/
}
