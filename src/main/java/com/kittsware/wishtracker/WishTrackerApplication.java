package com.kittsware.wishtracker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WishTrackerApplication {

    public static void main(String[] args) {
        SpringApplication.run(WishTrackerApplication.class, args);
    }

}
