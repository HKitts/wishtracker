package com.kittsware.wishtracker.controllers;

import com.kittsware.wishtracker.entities.User;
import com.kittsware.wishtracker.services.UserService;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

@RestController
public class UserController {
    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("/signup")
    public User registerNewUser(@RequestBody User user) {
        return this.userService.registerNewUser(user);
    }

    //ADMIN ONLY
    //TODO: Refactor this out so that nobody can use it until ADMIN accounts are setup
    @GetMapping("/users")
    public List<User> getAllUsers() {
        return this.userService.getAllUsers();
    }

    /*
    @GetMapping("/event/{eventId}/owner")
    public User getWishEventOwner(@PathVariable Long eventId) {
        return this.userService.getWishEventOwner(eventId);
    }
     */

    //TODO: Refactor to use Response Entity
    @DeleteMapping("/user")
    public boolean deleteUser(Principal principal) {
        return this.userService.deleteUser(principal.getName());
    }

    //TODO: What do you actually need to send back to the User?
    /*
        When I delete a User I need to make sure all events they owned get removed, but any events they didnt own but
        were a part of need to have them removed as participants

        Deleting a User should do 4 things
            1) Delete all WishEvents you own
            2) Delete all WishItems you own
            3) Remove you as a Participant from the WishEvents you were a Participant ONLY
            4) Remove any User related data like username/password

        Spring Data JPA DeleteBy Method will do 1, part of 2, and 4. You will probably need something more custom that
        does everything for you.

       //TODO: Make sure there is a Delete Wish Event(s) by Owner function
       //TODO: Make sure there is a Delete Wish Item(s) by Owner function
       //TODO: Make sure there is a Remove Participant User from Event function. This means removing all reference of a NON-OWNER from an Event, This would include

       When you delete an Event, you don't want to delete any Associated Users,
       When you delete a User, you want to delete Events they owned, but not ones they didn't.
     */

    /*
    TODO:
        Event Owner wants to remove a User from an Event
        User wants to leave an Event
        Removed User still has account post-removal
        Need to be able to Remove all User reference from an Event without removing the User
        This would be any Participant Objects right now
        However, what if removed User has bought a Wish Item?
     */
}
