package com.kittsware.wishtracker.controllers;

import com.kittsware.wishtracker.entities.Participant;
import com.kittsware.wishtracker.services.ParticipantService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ParticipantController {
    private final ParticipantService participantService;

    public ParticipantController(ParticipantService participantService) {
        this.participantService = participantService;
    }

    @GetMapping("/admin/parts")
    public List<Participant> getAllParticipants() {
        return this.participantService.getAllParticipants();
    }


}
