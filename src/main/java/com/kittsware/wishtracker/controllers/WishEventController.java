package com.kittsware.wishtracker.controllers;

import com.kittsware.wishtracker.entities.Participant;
import com.kittsware.wishtracker.entities.User;
import com.kittsware.wishtracker.entities.WishEvent;
import com.kittsware.wishtracker.entities.WishItem;
import com.kittsware.wishtracker.services.WishEventService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;
import java.util.Optional;

@RestController
public class WishEventController {

    private static final Logger logger = LoggerFactory.getLogger(WishEventController.class);
    private final WishEventService wishEventService;

    public WishEventController(WishEventService wishEventService) {
        this.wishEventService = wishEventService;
    }

    //ADMIN
    //TODO: You should add USER / ADMIN Authorities to the application. That way you can then make it so only certain
    //      User accounts can access the ADMIN endpoints. When it comes time to do that you can utilize the @PreAuthorize
    //      annotation to validate the User from Spring Security has the ADMIN Authority.
    @GetMapping(value = "/admin/events")
    public List<WishEvent> getAllWishEvents() {
        return this.wishEventService.getAllWishEvents();
    }

    //GET all WishEvents for the current User
    @GetMapping("/events")
    public List<WishEvent> getWishEventsForAuthUser(Principal principal) {
        return this.wishEventService.getWishEventsForAuthUser(principal.getName());
    }

    @PostMapping(value = "/event")
    public WishEvent addNewEvent(Principal principal, @RequestBody WishEvent wishEvent) {
        logger.info("USER: " + principal.getName());

        return this.wishEventService.createNewEvent(principal.getName(), wishEvent);
    }

    @PostMapping("/event/{eventId}/part")
    public boolean addParticipant(@PathVariable Long eventId, @RequestBody Participant participant, Principal principal) {
        return this.wishEventService.addNewParticipant(eventId, principal.getName(), participant);
    }

    /*
        PARTICIPANT DELETIONS
     */
    //This function is called when you as a Participant want to leave an Event you aren't the owner of.
    @DeleteMapping("/event/{eventId}/participant")
    public void deleteYourselfFromEvent(@PathVariable Long eventId, Principal principal) {
        //TODO: Create functionality
    }

    //This function is called when Participant to be removed isn't the Event Owner.
    @DeleteMapping("/event/{eventId}/owner/participant")
    public void deleteParticipant(@PathVariable Long eventId, @RequestBody Participant participant, Principal principal) {
        /*
            What if the Participant in question has their ID attached? Can't I just remove based on ID?
         */
        //TODO: Make sure Participant data is valid.
        //TODO: Refactor this to change the return class to Response Entity. Right now we don't care if the Participant is removed or not
        this.wishEventService.deleteParticipantFromEvent(eventId, participant, principal.getName());
    }

    //I think there are 3 instances a Participant would ever be deleted.
    //  1: Participant removes themselves from an Event they don't Own
    //  2: Event Owner removes a Participant that isn't themself (YOU HAVE THIS DONE)
    //  3: Event Owner removes a Participant that is themself




    @GetMapping(value = "/events/{eventId}")
    public Optional<WishEvent> getWishEventById(@PathVariable Long eventId) {
        return this.wishEventService.getWishEventById(eventId);
    }

    //Function to get all Users that are Participants for a given Event
    //TODO: Refactor to remove this later.
    @GetMapping("/event/{eventId}/users")
    public List<Participant> getAllEventParticipants(@PathVariable Long eventId, Principal principal) {
        //TODO: Refactor to include the current User to verify they have authority to access this event
        return this.wishEventService.getAllEventParticipants(eventId, principal.getName());
    }

    @DeleteMapping("/event/{eventId}")
    public void deleteEvent(@PathVariable Long eventId, Principal principal) {
        this.wishEventService.deleteWishEvent(eventId, principal.getName());
    }

    /*@GetMapping("/event/{eventId}/owner")
    public User getWishEventOwner(@PathVariable Long eventId) {
        return this.wishEventService.getWishEventOwner(eventId);
    }*/

    //Edit the EVent Owner for an Event you own, but stay in the Event.
    @PutMapping("/event/{eventId}/owner/change")
    public WishEvent updateEventOwnerAndStay(@PathVariable Long eventId, @RequestBody Participant newOwner, Principal principal) {
        return this.wishEventService.updateEventOwnerAndStay(eventId, newOwner, principal.getName());
    }

    //This method will allow an Event Owner to leave an Event they Own
    @PutMapping("/event/{eventId}/owner/leave")
    public void updateEventOwnerAndLeave(@PathVariable Long eventId, @RequestBody Participant newOwner, Principal principal) {
        //TODO: Refactor this method to return a different Type
        this.wishEventService.updateEventOwnerAndLeave(eventId, newOwner, principal.getName());
    }

    //This method will allow an Event Owner to edit minor details of an Event
    @PutMapping("/event/{eventId}/owner/edit")
    public void updateEventDetails(@PathVariable Long eventId, @RequestBody WishEvent wishEvent, Principal principal) {
        //TODO: REfactor this method to return a different Type
        this.wishEventService.updateWishEventDetails(eventId, wishEvent, principal.getName());
    }

    //This method will allow a Participant to Leave an Event safely
    //@PutMapping("/event/{eventId}/leave")



}
