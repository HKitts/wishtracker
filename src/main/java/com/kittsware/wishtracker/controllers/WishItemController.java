package com.kittsware.wishtracker.controllers;

import com.kittsware.wishtracker.entities.WishItem;
import com.kittsware.wishtracker.entities.adapters.WishItemOnly;
import com.kittsware.wishtracker.services.WishItemService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

@RestController
//TODO: Create a RequestMapping default URI
public class WishItemController {
    private final WishItemService wishItemService;
    private static final Logger logger = LoggerFactory.getLogger(WishItemController.class);

    public WishItemController(WishItemService wishItemService) {
        this.wishItemService = wishItemService;
    }

    //ADMIN API
    @GetMapping(value = "/admin/wishitems")
    public List<WishItem> getAdminWishItems() {
        return this.wishItemService.getAllWishItems();
    }

    //This controller will get all wish items for the current authenticated user.
    //Return Type will not be Wish Item but the new item type
    @GetMapping(value = "/wishitems")
    public Collection<WishItemOnly> getWishItemsForCurrentUser() {
        //Hard coding the Current User until we can get the Spring Security stood up.
        //TODO: Refactor this call so that the WishItemOwner parameter gets replaced with the Authenticated User from the Spring Security Context
        return this.wishItemService.getWishItemsForOwner(1L);
    }

    @GetMapping(value = "/wishitems/{wishItemOwnerId}")
    public Optional<List<WishItem>> getWishItemsForFriend(@PathVariable Long wishItemOwnerId) {
        //TODO: Refactor this call so that the requester parameter gets replaced with the Authenticated User from the Spring Security Context.
        //Because this returns an optional, there will never be an error thrown if the request failed.
        return this.wishItemService.findWishItemsForUser(wishItemOwnerId, 1L);
    }

    @DeleteMapping(value = "/wishitem/{wishItemId}")
    public void removeWishItem(@PathVariable Long wishItemId) {
        //TODO: Alert Buyers that their item was removed from the list.
        this.wishItemService.deleteWishItem(wishItemId);
    }

    /*
    @PostMapping(value = "/event/{eventId}/newitem")
    public WishItem addNewWishItem(@PathVariable Long eventId, @RequestBody WishItem wishItem) {
        //TODO: Check that the current authenticated User has access to the eventID before continuing.
        return this.wishItemService.addWishItem(eventId, wishItem);
    } */

    //New Challenge: Because you aren't attaching a unique participant to an event, the current username will have multiple participants
    //               and you don't know which Event the Item to be Added belongs to. If you have the EventID attached as part of the request
    //               you can then check for Participants that match the username from Spring & the EventID passed. With the Participant,
    //               add a new Item.
    @PostMapping("/event/{eventId}/newitem")
    public WishItem addNewWishItem(@PathVariable Long eventId, @RequestBody WishItem wishItem, Principal principal) {
        return this.wishItemService.addWishItem(eventId, principal.getName(), wishItem);
    }

    /*//TODO: Take a look at this and figure out how to replicate for EventOwner
    @GetMapping(value = "/event/{eventId}/items")
    public List<WishItem> getWishItemsForEvent(@PathVariable Long eventId) {
        //TODO: Check that current authenticated User has access to the eventID before continuing.

            //The issue is that one single user will want to see all Items, including their own, which should be of type WishItemOnly
            //We just have to do one get for all of items where the current User is/isn't the owner. The UI will handle the organization of the different lists

        return this.wishItemService.findWishItemsForEvent(eventId);
    }*/

    //What happens when the current user does and Update or Delete to an existing non-WishItem?
    //The actual function calls to modify/remove the real WishItems is easy because we have the Item's ID in the
    //non-WishItem object. However, the problem is updating what the user sees on screen, which has changed data now.
    //You could always just have another GET call done after the Update/Delete happens to reload the list. This will
    //perform worse though due to 2 network calls. I thought about doing a cache, but the problem with that is the cache
    //is linked to the running instance of the application, not the user, unless I were to introduce threading. But even
    //with threading I think that approach is not feasible, due to memory constraints on the running application.

    /**
     * The other thought is to just have the client side worry about the filtering and abstraction of information.
     * The main problem with that is the data isn't hidden from view if someone were smart enough to make API calls. This
     * would lead to the user being able to see if their items were bought, before that information is filtered out.
     *
     * I can't think of any other way around this other than to make another GET call to refresh the client data whenever
     * an update or removal happens to the list. The service will transform the data returned from the backend into a new
     * list of data, totally separated from the DB at that point.
     *
     * I guess I could make the edit/delete of an object to the client table as well as the backend. But this could lead
     * to several problems of its own.
     *
     * I wonder if theres a way to abstract the information that is returned by the backend, so only specific columns are
     * returned based on certain calls. If that is the case and JPA allows me to map the truncated data to the same WishItem
     * object, then I wouldn't have to worry about this because the data would just have null values for the sensitive info.
     * This also means I would only need one network call which should save some performance.
     */
}
