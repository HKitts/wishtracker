package com.kittsware.wishtracker.services;

import com.kittsware.wishtracker.entities.Participant;
import com.kittsware.wishtracker.entities.User;
import com.kittsware.wishtracker.entities.WishEvent;
import com.kittsware.wishtracker.entities.WishItem;

import java.util.List;
import java.util.Optional;

public interface WishEventService {
    List<WishEvent> getAllWishEvents();
    Optional<WishEvent> getWishEventById(Long wishEventId);

    void deleteWishEvent(Long eventId, String currentUser);

    void deleteAllByEventOwner(String eventOwner);

    //TODO: Create function to remove Participant from Event. AKA Delete a Participant
    //  do I need a WishEvent? Yes. You already have a repo function to delete by Event/Name. Just need to get Event

    boolean deleteParticipantFromEvent(Long eventId, Participant participant, String currentUser);

    //WishItem addWishItem(Long eventId, WishItem wishItem);
    List<Participant> getAllEventParticipants(Long eventId, String username);

    WishEvent createNewEvent(String currentUser, WishEvent wishEvent);
    List<WishEvent> getWishEventsForAuthUser(String username);

    boolean addNewParticipant(Long wishEventId, String currentUser, Participant participant);

    WishEvent updateEventOwnerAndStay(Long eventId, Participant newOwner, String currentUser);

    boolean updateEventOwnerAndLeave(Long eventId, Participant newOwner, String currentUser);

    WishEvent updateWishEventDetails(Long eventId, WishEvent wishEvent, String currentUser);
}
