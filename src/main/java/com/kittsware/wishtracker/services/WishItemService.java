package com.kittsware.wishtracker.services;

import com.kittsware.wishtracker.entities.WishItem;
import com.kittsware.wishtracker.entities.adapters.WishItemOnly;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

public interface WishItemService {
    //What are the common WishItem functions
    WishItem saveWishItem(WishItem wishItem);
    void deleteWishItem(Long wishItemId);
    List<WishItem> getAllWishItems();

    //the requester parameter is the user requesting the
    Optional<List<WishItem>> findWishItemsForUser(Long wishItemOwner, Long requester);

    //This function gets an owner's items that have been truncated.
    Collection<WishItemOnly> getWishItemsForOwner(Long wishItemOwner);

    WishItem addWishItem(Long eventId, String currentUser, WishItem wishItem);

    //List<WishItem> findWishItemsForEvent(Long eventId);
    List<WishItem> findWishItemsForParticipant(Long participantId);
}
