package com.kittsware.wishtracker.services;

import com.kittsware.wishtracker.entities.User;

import java.util.List;

public interface UserService {
    User registerNewUser(User user);

    boolean deleteUser(String username);

    List<User> getAllUsers();

    //Need to be able to pass in old password and new password
    //User updateUserPassword(String currentUser, String oldPassword, String password);
}
