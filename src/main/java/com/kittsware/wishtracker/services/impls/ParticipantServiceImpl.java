package com.kittsware.wishtracker.services.impls;

import com.kittsware.wishtracker.entities.Participant;
import com.kittsware.wishtracker.entities.WishEvent;
import com.kittsware.wishtracker.repositories.ParticipantRepository;
import com.kittsware.wishtracker.services.ParticipantService;
import com.kittsware.wishtracker.services.WishEventService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ParticipantServiceImpl implements ParticipantService {

    private final static Logger logger = LoggerFactory.getLogger(ParticipantServiceImpl.class);
    private final ParticipantRepository participantRepository;
    //private final WishEventService wishEventService;

    /*public ParticipantServiceImpl(ParticipantRepository participantRepository, WishEventService wishEventService) {
        this.participantRepository = participantRepository;
        this.wishEventService = wishEventService;
    }*/

    public ParticipantServiceImpl(ParticipantRepository participantRepository) {
        this.participantRepository = participantRepository;
    }

    @Override
    public Participant createNewParticipant(Participant participant) {
        logger.info("EVENT VALUE 2: "+participant.getParticipantEvent());
        //I am assuming the WishEvent for participant has been added already.
        //TODO: Refactor so that you check that participant has a WishEvent, which you can't really do.
        //The save() will always make sure the parameter object exists before executing the save.
        //if it is null, there will be an IllegalArgumentException thrown.
        return this.participantRepository.save(participant);
    }

    @Override
    public List<Participant> getAllParticipants() {
        return this.participantRepository.findAll();
    }

    @Override
    public Optional<Participant> getParticipantById(Long participantId) {
        return this.participantRepository.findById(participantId);
    }

    @Override
    public Optional<Participant> getParticipantByEventAndName(String participantName, WishEvent participantEvent) {
        return this.participantRepository.getParticipantByParticipantNameAndParticipantEvent(participantName, participantEvent);
    }

    @Override
    public boolean deleteAllByParticipantName(String participantName) {
        //Delete All Participants with ParticipantName = participantName
        return this.participantRepository.deleteParticipantsByParticipantName(participantName) > 0;
    }

    @Override
    public boolean deleteParticipantNonOwner(String participantName, WishEvent wishEvent) {
        //Should this function be used as a general remove Participant from Event? If yes, then you don't need to make sure the Participant
        //Name and Wish Event Owner Name aren't the same.

        //TODO: Do some validation checks.

        if (this.participantRepository.deleteParticipantByParticipantNameAndParticipantEvent(participantName, wishEvent) > 0) {
            return true;
        }
        return false;
    }

    @Override
    public boolean deleteParticipantOwner(Long participantId) {
        //TODO: Refactor this function to not leverage a boolean return type
        //Make sure the ParticipantId Exists (Even though the Event Service should've checked this too
        if(!this.participantRepository.existsById(participantId)) {
            return false;
        }

        //Make sure to delete the Participant
        this.participantRepository.deleteById(participantId);
        return true;
    }

    @Override
    public List<Participant> findParticipantsByName(String name) {
        return this.participantRepository.findParticipantsByParticipantName(name);
    }
}
