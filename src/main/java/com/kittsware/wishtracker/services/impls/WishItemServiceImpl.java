package com.kittsware.wishtracker.services.impls;

import com.kittsware.wishtracker.entities.Participant;
import com.kittsware.wishtracker.entities.WishEvent;
import com.kittsware.wishtracker.entities.WishItem;
import com.kittsware.wishtracker.entities.adapters.WishItemOnly;
import com.kittsware.wishtracker.repositories.WishEventRepository;
import com.kittsware.wishtracker.repositories.WishItemRepository;
import com.kittsware.wishtracker.services.ParticipantService;
import com.kittsware.wishtracker.services.WishEventService;
import com.kittsware.wishtracker.services.WishItemService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class WishItemServiceImpl implements WishItemService {
    private static final Logger logger = LoggerFactory.getLogger(WishItemServiceImpl.class);
    private final WishItemRepository wishItemRepository;
    //private final WishEventRepository wishEventRepository;
    private final WishEventService wishEventService;
    private final ParticipantService participantService;

    public WishItemServiceImpl(WishItemRepository wishItemRepository, ParticipantService participantService, WishEventService wishEventService) {
        this.wishItemRepository = wishItemRepository;
        this.participantService = participantService;
        this.wishEventService = wishEventService;
    }

    @Override
    public void deleteWishItem(Long wishItemId) {
        //TODO: refactor to return a ResponseEntity with appropriate codes
        //TODO: Add in check to see if the item exists.
        //TODO: Add in a check that the wishItemID matches an item owned by authenticated user
        this.wishItemRepository.deleteById(wishItemId);
    }

    @Override
    public Collection<WishItemOnly> getWishItemsForOwner(Long wishItemOwner) {
        return this.wishItemRepository.findWishItemsByWishItemOwner(wishItemOwner);
    }

    //This is the function that returns a full list, including if someone has bought an item. Make sure wishItemOwner isn't the requester.
    //Requester is the currently authenticated user.
    @Override
    public Optional<List<WishItem>> findWishItemsForUser(Long wishItemOwner, Long requester) {
        //You should check for equality and friendship here. this is because that could be considered business logic.
        //The return type should change to a Response Entity
        /*
            These are the possible outcomes of this function
                1) The WishItemOwner and the Requester are the same person
                    a) Return a failure of some sort. (Unauthorized Access)
                2) The WishItemOwner and the Requester aren't the same person and they are friends
                    a) Return the WishItemOwners List of Wish Items and a valid response code
                    b) Return a failure of some sort. (Unauthorized Access)

         */
        //TODO: Make check of equality between wishItemOwner and the Requester. If equal, return null. If different, call findAllByWishItemOwner
        //TODO: Make check of friendship between wishItemOwner and Requester
        return this.wishItemRepository.findAllByWishItemOwner(wishItemOwner);
    }

    @Override
    public List<WishItem> getAllWishItems() {
        return this.wishItemRepository.findAll();
    }

    @Override
    public WishItem saveWishItem(WishItem wishItem) {
        return this.wishItemRepository.save(wishItem);
    }

    /*@Override
    public WishItem addWishItem(Long eventId, WishItem wishItem) {
        WishEvent wishEvent = this.wishEventService.getWishEventById(eventId).orElse(null);
        if (null == wishEvent) {
            return null;
        }
        wishItem.setWishItemEvent(wishEvent);
        return this.wishItemRepository.save(wishItem);
    }*/

    @Override
    public WishItem addWishItem(Long eventId, String currentUser, WishItem wishItem) {
        //Need to get the ParticipantID from the eventId. Will need to get current User though.
        //Get the event first. Now you have the Event with the List of unique participant rows. Search the Participant for the one with the Username == currentUser.
        WishEvent event = this.wishEventService.getWishEventById(eventId).orElse(null);
        //Make sure event isn't null
        if (null == event) {
            return null;
        }


        Participant participant = this.participantService.getParticipantByEventAndName(currentUser, event).orElse(null);
        if (null == participant) {
            return null;
        }
        wishItem.setWishItemParticipant(participant);
        return this.wishItemRepository.save(wishItem);
    }

    @Override
    public List<WishItem> findWishItemsForParticipant(Long participantId) {
        Participant participant = this.participantService.getParticipantById(participantId).orElse(null);
        if (null == participant) {
            return null;
        }
        return this.wishItemRepository.findAllByWishItemParticipant(participant);
    }
}
