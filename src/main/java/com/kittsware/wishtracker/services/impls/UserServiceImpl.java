package com.kittsware.wishtracker.services.impls;

import com.kittsware.wishtracker.entities.User;
import com.kittsware.wishtracker.repositories.UserRepository;
import com.kittsware.wishtracker.services.ParticipantService;
import com.kittsware.wishtracker.services.UserService;
import com.kittsware.wishtracker.services.WishEventService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;
    private final ParticipantService participantService;
    private final WishEventService wishEventService;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    public UserServiceImpl(UserRepository userRepository, ParticipantService participantService, WishEventService wishEventService, BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.userRepository = userRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
        this.participantService = participantService;
        this.wishEventService = wishEventService;
    }

    @Override
    public User registerNewUser(User user) {
        if (this.userRepository.findUserByUsername(user.getUsername()).isPresent()) {
            //If the User Repository finds someone with the same Username, that means we need to have them change the new username
            //Eventually the username will be an email address.
            //TODO: Make the username an email variable.
            return null;
        }
        User tmpUser = new User();
        tmpUser.setUsername(user.getUsername());
        tmpUser.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        return this.userRepository.save(tmpUser);
    }


    @Transactional
    @Override
    public boolean deleteUser(String username) {
        //TODO: Validate username matches current Authenticated User (Might not be needed)
        //TODO: Refactor this to return something other than boolean.
        //TODO: Make sure anything associated with this User gets deleted.
        //You need to make sure All Participant with this username get removed

        //When you delete a User you want to make sure any Events that they Own and WANT to delete are removed. This will include all Participants & Wish Items & the Event itself
        //You want to make sure any Participant objects that are associated with the User being Deleted are removed. This will include all Wish Items associated with the Participant. The Event won't be deleted.

        //For Every Participant, we could have a boolean variable be something like isOwner.
        //If the Participant is the eventOwner, we mark that as true
        //If the the Participant to be Deleted is marked as Event Owner, delete the Event too? This won't work because you're not actually getting the Participant object from backend during deletion.

        //Call to Delete all Events that this User Created (Assuming they didn't want to transfer ownership)
        this.wishEventService.deleteAllByEventOwner(username);

        //Call to Delete all Participant and WishItems
        this.participantService.deleteAllByParticipantName(username);

        //TODO: Make a call to a function that updates any WishItems the User was the Buyer of.
        //TODO: Make sure other Users of the Event you Bought an Item for are aware that you returned the Item

         //Deleting all Participation should delete all Wish Items right? When would a User have WishItems that aren't
         //connected to them? THe answer is when they have Bought a Wish Item for another User.

        //You will probably need a call to delete all Wish Items too, or rather
        //TODO: Figure out what should happen when a User gets removed from an event, but the User has bought items for the Event
        //  Obviously need to change all WishItems that the removed User has bought so that isBought is false again, and the Buyer is null again.
        //  You will want to send a Notification to the remaining participants that the User was removed (Nothing more, because then everyone will know they bought something)
        //TODO: Also figure out what happens when the User's Items are removed and some were bought
        //  You should send a Notification to the Buying User that the User was removed, that way they can make a decision about returning the item.



        //How would you trade for a living? Realistically how much do you need to make a month for that to be viable?

        if (this.userRepository.findUserByUsername(username).isPresent()) {
            return this.userRepository.deleteByUsername(username) > 0;
        }
        return false;
    }

    /*@Override
    public User updateUserPassword(String currentUser, String oldPassword, String password) {
        //TODO: Update password requirements so that it is harder to have very similar new passwords to old ones
        //TODO: Update so that there is a history of old passwords for reference
        //TODO: Update so that you have to decrypt the passwords from the UI.
        //TODO: Check that currentUser is the same as current authenticated User (Might be unnecessary)
        //TODO: Validate that the oldPassword and password aren't null (Use @NotNull annotation)
        if (oldPassword.equals(password)) {
            return null;
        }
        User curUser = this.userRepository.findUserByUsername(currentUser).orElse(null);
        if (null == curUser) {
            return null;
        }
        curUser.setPassword(bCryptPasswordEncoder.encode(password));
        //You are not changing anything else aside from the password. You didn't create a new repository function
        //because you think the provided save will work for edits.
        //TODO: Deauthenticate the current User
        return this.userRepository.save(curUser);
    }
     */

    @Override
    public List<User> getAllUsers() {
        return this.userRepository.findAll();
    }

}
