package com.kittsware.wishtracker.services.impls;

import com.kittsware.wishtracker.entities.Participant;
import com.kittsware.wishtracker.entities.User;
import com.kittsware.wishtracker.entities.WishEvent;
import com.kittsware.wishtracker.repositories.UserRepository;
import com.kittsware.wishtracker.repositories.WishEventRepository;
import com.kittsware.wishtracker.services.ParticipantService;
import com.kittsware.wishtracker.services.WishEventService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class WishEventServiceImpl implements WishEventService {
    private static final Logger logger = LoggerFactory.getLogger(WishEventServiceImpl.class);

    private final WishEventRepository wishEventRepository;
    private final ParticipantService participantService;

    public WishEventServiceImpl(WishEventRepository wishEventRepository, ParticipantService participantService) {
        this.wishEventRepository = wishEventRepository;
        this.participantService = participantService;
    }

    @Override
    public WishEvent createNewEvent(String currentUser, WishEvent wishEvent) {
        //Check that the currentUser matches the Spring Security Context User again.
        if (isUserNotValid(currentUser)) {
            return null;
        }

        //Make sure there isn't an ID for the new WishEvent.
        //TODO: Refactor this to not do an explicit null check.
        if (wishEvent.getWishEventId()!=null) {
            return null;
        }

        //If current User is valid, then we need to add the username as a variable to the Event Object.
        wishEvent.setWishEventOwnerName(currentUser);
        WishEvent tmpEvent = this.wishEventRepository.save(wishEvent);

        //TODO: Make sure the save() happened successfully first.

        //Make sure you are added as a participant
        Participant participant = new Participant(currentUser);
        participant.setParticipantEvent(tmpEvent);

        //TODO: Make sure the creation of a new Participant happens
        this.participantService.createNewParticipant(participant);

        return tmpEvent;
    }

    @Override
    public boolean addNewParticipant(Long wishEventId, String currentUser, Participant participant) {
        //Check that currentUser matches the Spring Security Context User again
        if (isUserNotValid(currentUser)) {
            return false;
        }

        WishEvent tmpEvent = this.wishEventRepository.findWishEventByWishEventId(wishEventId).orElse(null);
        //Check that the Event exists
        if (null == tmpEvent) {
            return false;
        }

        //Check if the currentUser is not the Event's Owner
        if (!tmpEvent.getWishEventOwnerName().equals(currentUser)) {
            return false;
        }

        //Check if the Participant Name and currentUser are the same
        if (participant.getParticipantName().equals(currentUser)) {
            return false;
        }

        participant.setParticipantEvent(tmpEvent);

        //TODO: Make sure the creation of a new Participant happens
        this.participantService.createNewParticipant(participant);
        return true;
    }

    @Override
    public List<WishEvent> getAllWishEvents() {
        return this.wishEventRepository.findAll();
    }

    @Override
    public Optional<WishEvent> getWishEventById(Long wishEventId) {
        //This code below is good when you already know the WishEvent with wishEventId exists.
        //return this.wishEventRepository.getOne(wishEventId);
        return this.wishEventRepository.findWishEventByWishEventId(wishEventId);
    }

    @Override
    public List<WishEvent> getWishEventsForAuthUser(String username) {
        //Create a List of Events to be returned
        List<WishEvent> tmpEvents = new ArrayList<>();

        //Get all of the current User's Participant
        List<Participant> tmp = participantService.findParticipantsByName(username);

        //If no Participants are returned then the current User doesn't belong to any Events.
        if (tmp.isEmpty()) {
            return null;
        }

        for (Participant p : tmp) {
            //p should now have the entire WishEvent
            tmpEvents.add(p.getParticipantEvent());
        }
        return tmpEvents;
    }

    @Override
    public List<Participant> getAllEventParticipants(Long eventId, String username) {
        //Participant IDs are visible when you get an Event's Participants. The question is now if that is okay or not...

        //What needs done exactly? I get the Event in question, make sure the current User is a Participant, get
        //a List of Participants (Which contain their Wish Items). Make sure the current User's Participant object only
        //has the WishItemOnly objects instead of the full Wish Item.

        //Check that username is still the current authenticated User
        if (isUserNotValid(username)) {
            return null;
        }

        //Get the Wish EVent in question, make sure it exists.
        WishEvent wishEvent = this.wishEventRepository.findWishEventByWishEventId(eventId).orElse(null);
        if (null == wishEvent) {
            return null;
        }

        //TODO: Make sure current User is a Participant of this Event. I think there should be a private helper function
        //  in the Participant Service where you pass a username / event and you see if the username is a participant as
        //  a boolean. Maybe it is a function inside of WishEvent? Honestly I dont like using the Getter/Setter functions
        //  of the Entity classes. This makes me feel like I will now need to unit test the entity class.

        //Check if username actually has a Participant for this Event.
        if (this.participantService.getParticipantByEventAndName(username, wishEvent).isEmpty()) {
            return null;
        }

        //Here I get the Participants via the Object, but what if I delete them from the Objects?
        //TODO: 5.12.21 7:14PM - Can you see Participant IDs if you get via the Participant Service?
        return wishEvent.getWishEventParticipants();
    }

    @Override
    public void deleteWishEvent(Long eventId, String currentUser) {
        //TODO: Eventually check that the current user is the Event Host
        //TODO: Make sure current User Exists
        //Make sure currentUser is still the Authenticated User
        if (isUserNotValid(currentUser)) {
            return;
        }

        //Make sure the eventID actually has an Event
        WishEvent tmpEvent = this.wishEventRepository.findWishEventByWishEventId(eventId).orElse(null);
        if (null == tmpEvent) {
            return;
        }

        //Make sure Event Owner and Current User are the same
        if (!tmpEvent.getWishEventOwnerName().equals(currentUser)) {
            return;
        }

        //Deleting an Event will delete all associated Participants and any of their Wish Items

        this.wishEventRepository.deleteById(eventId);
    }

    @Override
    public void deleteAllByEventOwner(String eventOwner) {
        long tmp = this.wishEventRepository.deleteWishEventsByWishEventOwnerName(eventOwner);
        logger.info("THIS IS HOW MANY EVENTS WERE DELETED: "+tmp);
    }

    //TODO: Create functionality to get a List of all WishItems the Participant is a Buyer of. You have to be able to edit the
    //      WishItems that the Participant has bought. Get the List, edit each one, finish Participant Deletion.

    @Override
    public boolean deleteParticipantFromEvent(Long eventId, Participant participant, String currentUser) {
        //TODO: Refactor so you just delete the Participant by ID. Think about if you want to expose the Participant ID to the UI.

        //check that the currentUser is still the authenticated User
        if (isUserNotValid(currentUser)) {
            return false;
        }

        //Check that CurrentUser and Participant aren't the same. This function is to remove a Participant that isnt the Event Owner
        if (participant.getParticipantName().equals(currentUser)) {
            return false;
        }

        //Check that the WIshEvent exists
        WishEvent tmpEvent = this.wishEventRepository.findWishEventByWishEventId(eventId).orElse(null);
        if (null == tmpEvent) {
            return false;
        }

        //Check if currentUser isn't the Event Owner
        if (!tmpEvent.getWishEventOwnerName().equals(currentUser)) {
            return false;
        }

        //delete the Participant, return the result of deletion.
        //TODO: Refactor this function so that we return Response Entity so more options can be given.
        return this.participantService.deleteParticipantNonOwner(participant.getParticipantName(), tmpEvent);
    }

    @Override
    public WishEvent updateEventOwnerAndStay(Long eventId, Participant newOwner, String currentUser) {
        //Make sure Participant and currentUser are not the same
        if (newOwner.getParticipantName().equals(currentUser)) {
            return null;
        }

        //Make sure the eventID actually has an Event
        WishEvent tmpEvent = this.wishEventRepository.findWishEventByWishEventId(eventId).orElse(null);
        if (null == tmpEvent) {
            return null;
        }

        //Check that the currentUser is the Event's Owner
        if (!tmpEvent.getWishEventOwnerName().equals(currentUser)) {
            return null;
        }

        //Check that the newOwner is actually an Event Participant
        //I have written 3 different functions that essentially do the same thing. I need to be able to figure out which is more efficient.
        //I think the Participant object might not have an ID attached to it. So it might be better to check the event IDs instead?
        if (newOwner.getParticipantEvent().getWishEventId().equals(eventId)) {
            return null;
        }
        /*if (!newOwner.getParticipantEvent().equals(tmpEvent))
        {
            return null;
        }*/
        /*if (!isEventParticipant(tmpEvent, newOwner)) {
            return null;
        }*/

        //Set the Event's new Owner
        tmpEvent.setWishEventOwnerName(newOwner.getParticipantName());

        //Calling save with edited information should update the DB row, not add a new one.
        return this.wishEventRepository.save(tmpEvent);
    }

    @Override
    public boolean updateEventOwnerAndLeave(Long eventId, Participant newOwner, String currentUser) {
        //TODO: Refactor this function to be more efficient. Like do you need to get the whole Event Object?
        //      What if there is a lot of associated data? That call to get the object could take some time.
        //Make sure Participant and currentUser are not the same
        if (newOwner.getParticipantName().equals(currentUser)) {
            return false;
        }

        //Make sure the Event exists
        WishEvent tmpEvent = this.wishEventRepository.findWishEventByWishEventId(eventId).orElse(null);
        if (null == tmpEvent) {
            return false;
        }

        //Make sure currentUser is the Event's Owner
        if (!tmpEvent.getWishEventOwnerName().equals(currentUser)) {
            return false;
        }

        //Make sure the newOwner is a Participant of the Event
        if (newOwner.getParticipantEvent().getWishEventId().equals(eventId)) {
            return false;
        }

        //TODO: Make sure a Special Notification is sent to all Buyers of your WishItems letting them know you've left

        //Make sure to remove your Participant Object for this Event
        if (!this.participantService.deleteParticipantNonOwner(currentUser, tmpEvent)) {
            //TODO: Refactor the returned result from deleteParticipant so that we know why the deletion didn't occur.
            return false;
        }

        //TODO: Make sure a Notification is sent too remaining Event Participants of your Departure

        //Make sure that WishItems you've Bought are updated
        /*
            This is a challenging thing to do. You have to go through all WishItems for this Event and if You are the
            Buyer of the Item, that Item needs edited. The nice thing, is doing this after you delete your Participant
            Objects because all of your associated WishItems will have been removed, so there will be less WishItems to
            search through. Right now, the Buyer is identified with a Long ID of their Participant Object.

            The only thing is that if we decide not to end up linking the Owner to the item. Which when thinking about
            can't actually happen because then bought WishItems' isBought variable will always remain true after being
            purchased. There will be no record to know which Participant bought which WishItem, so when a Participant
            leaves you can never know which items they have purchased.

            So how do you go about getting the WishItems? Do you have an EventService function that can get them? The
            WishItem class doesn't have a reference back to the Event, so the Participant will need involved in some way.
            If you get the WishItems by Buyer, you have the Items but you aren't yet sure of which ones belong to the Event
            You are leaving. You will have a list of every single WishItem you've bought across all Events. In a perfect
            world you are able to find all WishItems by Buyer & Event. You might be able to string together some nested
            functions to take the tmpEvent, get the Participants, get the Events, go through each one, manually make the
            update, and save the change. The problem with that is you want to do those actions via Service calls, not object
            calls. A quick workaround would be to make a dumb reference to the Event in the WishItem, kind of how we have
            the Buyer variable set up right now. If we had that variable, we should be able to get all WishItems by Buyer
            and Event. We make the EventIDs pretty public anyways. The alternative is use the Participant Service to get
            all Participants for the Event. Then for each Participant, use a WishItem Service to get all their WishItems
            and see if You are the Buyer. If you are then you use the WishItemService to make a call to "return" the item,
            which will change isBought to false, and Buyer back to null.
         */
        //TODO: Figure out the best way to update the WishItemBuyer when the Buyer leaves the Event

        //Make sure to update the tmpEvent's Owner

        //Make sure to save the tmpEvent with the new Owner

        return false;
    }

    @Override
    public WishEvent updateWishEventDetails(Long eventId, WishEvent wishEvent, String currentUser) {
        //Make sure the eventID actually has an Event
        WishEvent tmpEvent = this.wishEventRepository.findWishEventByWishEventId(eventId).orElse(null);
        if (null == tmpEvent) {
            return null;
        }

        //Check that the currentUser is the Event's Owner
        if (!tmpEvent.getWishEventOwnerName().equals(currentUser)) {
            return null;
        }

        //Update minor wishevent details
        tmpEvent.setWishEventName(wishEvent.getWishEventName());
        tmpEvent.setWishEventDescription(wishEvent.getWishEventDescription());

        //Calling save with edited information should update the DB row, not add a new one.
        return this.wishEventRepository.save(tmpEvent);
    }

    //I'm not sure how much I like this check. It is a String equality check which takes time.
    private boolean isUserNotValid(String username) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String currentUserName = authentication.getName();
        return !currentUserName.equals(username);
    }

    private boolean isEventParticipant(WishEvent wishEvent, Participant participant) {
        return wishEvent.getWishEventParticipants().contains(participant);
    }
}
