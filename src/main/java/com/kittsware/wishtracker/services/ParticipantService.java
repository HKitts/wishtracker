package com.kittsware.wishtracker.services;

import com.kittsware.wishtracker.entities.Participant;
import com.kittsware.wishtracker.entities.WishEvent;

import java.util.List;
import java.util.Optional;

public interface ParticipantService {

    List<Participant> getAllParticipants();

    Optional<Participant> getParticipantById(Long participantId);

    Optional<Participant> getParticipantByEventAndName(String participantName, WishEvent participantEvent);

    boolean deleteAllByParticipantName(String participantName);

    boolean deleteParticipantNonOwner(String participantName, WishEvent wishEvent);

    boolean deleteParticipantOwner(Long participantId);

    List<Participant> findParticipantsByName(String name);

    //This is an additional function to createNewParticipant for an Event Owner
    Participant createNewParticipant(Participant participant);
}
